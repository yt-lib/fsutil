import { join as pathjoin, dirname, basename } from 'path'
import { promises as fs, statSync } from 'fs'
import { decode } from 'iconv-lite'

const readJson = <T = unknown>(path: string) =>
	fs.readFile(path, 'utf-8').then(v => JSON.parse(v) as T)

const writeJson = <T = unknown>(path: string, content: T) =>
	fs.writeFile(path, JSON.stringify(content))

export class Directory {
	readonly type = 'Directory'
	constructor(public readonly path: string) {}
	toJSON() {
		return this.path
	}
	cat(name: string) {
		return this.file(name).read()
	}
	cd(name: string) {
		return new Directory(pathjoin(this.path, name))
	}
	ls() {
		return fs.readdir(this.path)
	}
	async getItem(name: string) {
		const s = await this.stat(name)
		if (s.isFile()) return this.file(name)
		if (s.isDirectory()) return this.cd(name)
		console.log('unsupported type', s)
	}
	async listAll() {
		const ls = await this.ls()
		return Promise.all(ls.map(async name => this.getItem(name)))
	}
	async *walk(): AsyncGenerator<Directory | File, void, unknown> {
		for (const name of await this.ls()) {
			const item = await this.getItem(name)
			if (!item) continue
			yield item
			if ('Directory' === item.type)
				for await (const tmp of item.walk()) yield tmp
		}
	}
	stat(name: string) {
		return fs.stat(pathjoin(this.path, name))
	}
	statSync(name: string) {
		return statSync(pathjoin(this.path, name))
	}
	file(name: string) {
		return new File(pathjoin(this.path, name))
	}
	write(name: string, content: string | Buffer) {
		return this.file(name).write(content)
	}
}

export class File {
	readonly type = 'File'
	readonly name: string
	constructor(public readonly path: string) {
		this.name = basename(path)
	}
	toJSON() {
		return this.path
	}
	dir() {
		return new Directory(dirname(this.path))
	}
	read() {
		return fs.readFile(this.path, 'utf-8')
	}
	readJson<T = unknown>() {
		return readJson<T>(this.path)
	}
	readBuffer(): Promise<Buffer> {
		return fs.readFile(this.path)
	}
	readsjis() {
		return fs.readFile(this.path).then(r => decode(r, 'shiftjis'))
	}
	write(content: string | Buffer) {
		return fs.writeFile(this.path, content)
	}
	writeJson(content: unknown) {
		return writeJson(this.path, content)
	}
}
